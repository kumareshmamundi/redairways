//Controller
var flight_controller = require('../controllers/flight_controller');
var booking_controller = require('../controllers/booking_controller');

//Routes
module.exports = (app)=>{
app.get("/flight/booking/:date",flight_controller.test);
app.post("/flight",flight_controller.create);
app.put("/flight/:id",flight_controller.update);
app.get('/flight',flight_controller.show);
app.get('/flight/:from/:to/:date/:class',flight_controller.showOne);
app.delete('/flight/:id',flight_controller.delete);
app.patch('/flight/:id/:from',flight_controller.updateOne);


app.post("/flight/booking",booking_controller.create);
app.put("/flight/booking/:id",booking_controller.update);
app.get("/flight/booking",booking_controller.show);
app.get("/flight/booking/:id",booking_controller.findBookingId);
app.delete("/flight/booking/:id",booking_controller.delete);

};

