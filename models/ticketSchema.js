//dependency
var mongoose = require('mongoose');
//schema
var ticketSchema=new mongoose.Schema({
	name:String,
	ticket:Number
});

//model
var ticket = mongoose.model("ticket",ticketSchema);

//export ticket model
module.exports=ticket;