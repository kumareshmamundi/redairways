
var mongoose = require('mongoose');

var passengerSchema = new mongoose.Schema({
	type: String,
	name: String,
	age: Number,
	gender: String,
	seatNo: Number,
	ticketNo:Number
});


var bookingSchema = new mongoose.Schema({
	bookingDate: Date,
	paymentMode: String,
	flightId:{
		type: mongoose.Schema.Types.ObjectId,
        ref: 'Flight'
	},
	date: Date,
	price: Number,
	agentName: String,
	pnrNo: Number,
	class: String,
	passenger:[passengerSchema],
	travelDate : Date
});

var Booking=mongoose.model('Booking',bookingSchema);

module.exports=Booking;