//dependency
var mongoose = require('mongoose');
//schema
var pnrSchema=new mongoose.Schema({
	name:String,
	pnr:Number
});

//model
var pnr = mongoose.model("pnr",pnrSchema);

//export pnr model
module.exports=pnr;