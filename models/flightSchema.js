var mongoose = require('mongoose');

var flightSchema = new mongoose.Schema({
	carrierName : String,
	flightName : String,
	from : String,
	to : String,
	operatingDays: String,
	departureTime: String,
	arrivalTime: String,
	duration: String,
	class:[{
		classType: String,
		price: Number,
		noOfSeats: Number,
		baggageDetails:[{
				adult: Number,
				child: Number,
				infant: Number
		}]
	}]
});

var Flight = mongoose.model("Flight",flightSchema);

module.exports=Flight;