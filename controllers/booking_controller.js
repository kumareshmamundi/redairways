//required models
var Booking = require('../models/bookingSchema');
var Flight = require('../models/flightSchema');
var pnr = require('../models/pnrSchema');
var ticket = require('../models/ticketSchema');

//excute result
//create
exports.create=(req,res)=>{
		//Auto increment pnrNo
		// ticket.find().then((result)=>console.log(result));
		// console.log(req.body.passenger.length);
		ticket.findOneAndUpdate({
				name:"ticket"
			},{$inc:{
				ticket: req.body.passenger.length+2
			}
			}).then(function(add){
				console.log(add);
			});
			pnr.findOneAndUpdate({
				name:"pnr"
			},{$inc:{
				pnr: 1
			}
			}).then(function(add){
				console.log(add);
			});

			// console.log(req.body.passenger.length);
			// console.log(req.body.passenger[0].ticketNo);


		//find value only from pnr
		const pnrno=pnr.distinct( "pnr" ).then(function(pnrNoArr){
			return pnrNoArr[1];
		});
		//find value only from ticketNo
		const ticketno=ticket.distinct( "ticket" ).then(function(ticketNoArr){
			return ticketNoArr[0];
		});
		
		//set pnr,ticketNo and create 
		async function getPnrTicketNo(){
			let pnr=await pnrno;
			let ticket=await ticketno;
			console.log("ve"+ticket);
			var passengerArr=[];
			for(var i=0;i<req.body.passenger.length;i++){
				ticket=ticket+1;
				passengerArr.push({
			        type: req.body.passenger[i].type,
			        name: req.body.passenger[i].name,
			        age: req.body.passenger[i].age,
					gender: req.body.passenger[i].gender,
					seatNo: req.body.passenger[i].seatNo,
					ticketNo: ticket
			    });
			    console.log("ulla"+ticket);
			    
			}
			console.log(passengerArr);
			
			Booking.create({
			bookingDate: new Date(),			
			paymentMode: req.body.paymentMode,
			flightId: req.body.flightId,
			price: req.body.price,
			agentName: req.body.agentName,
			pnrNo: pnr,
			class: req.body.class,
			passenger: passengerArr,
			travelDate: req.body.travelDate
		}).then(function(add){
				if(!add){
					res.status(400).send();
				}
				else{
					res.status(200).send(add);
									
				}
		});
		
		}

		//call function
		getPnrTicketNo();
		
	};
	exports.findBookingId=(req,res)=>{
		// Booking.find({ "travelDate" : req.params.id}).countDocuments()
		
		// // Booking.find( { travelDate: { $lte: new Date() } } ).estimatedDocumentCount()
		// 	.then(function(show){
		// 		if(!show){
		// 			res.status(400).send();
		// 		}
		// 		else{
		// 			// res.status(200).send(show);
		// 			// if(show===3){
		// 				console.log(show);
		// 			// }
		// 			// else{
		// 			// 	console.log("no show");
		// 			// }
					
		// 		}
		// 	});
		Booking.find({"_id":req.body.id})
		.then(function(show){
				if(!show){
					res.status(400).send();
				}
				else{
					res.status(200).send(show);
				}
			});

		
	};
	exports.show=(req,res)=>{
		//find pnr collection
		pnr.find().then(function(show){console.log(show)});
		
		//find booking collection
		Booking.find()
			.then(function(show){
				if(!show){
					res.status(400).send();
				}
				else{
					res.status(200).send(show);
				}
			});



	};
	// exports.showOne=(req,res)=>{
	// 	Flight.find({_id:req.params.id})
	// 		.then(function(show){
	// 			if(!show){
	// 				res.status(400).send();
	// 			}
	// 			else{
	// 				res.status(200).send(show);
	// 			}
	// 		});
	// };
	exports.update=(req,res)=>{
		Booking.findOneAndUpdate({_id: req.params.id},{$set:{
				bookingDate: new Date(),			
				paymentMode: req.body.paymentMode,
				flightId: req.body.flightId,
				price: req.body.price,
				agentName: req.body.agentName,
				pnrNo: req.body.pnrNo,
				class:req.body.class,
				passenger:req.body.passenger,
				travelDate: req.body.travelDate
			
			}},{new: true}
			
			).then(function(add){
				if(!add){
					res.status(400).send();
				}
				else{
					res.status(200).send(add);
				}
			});
	};
	exports.delete=(req,res)=>{
		Booking.findOneAndRemove({
			_id: req.params.id
		}).then(function(show){
			if(!show){
				res.status(400).send("cant delete");
			}else{
				res.status(200).send(show);
			}
		});
	
	};
	
	// exports.test=(req,res)=>{
	// 	Booking.find({ "travelDate" : req.params.date}).then((result)=>{console.log(result)});						 
	// };

