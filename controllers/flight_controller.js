var Flight = require('../models/flightSchema');
var Booking = require('../models/bookingSchema');

exports.create=(req,res)=>{
		Flight.create({
			carrierName : req.body.carrierName,
			flightName : req.body.flightName,
			from : req.body.from,
			to : req.body.to,
			operatingDays: req.body.operatingDays,
			departureTime: req.body.departureTime,
			arrivalTime: req.body.arrivalTime,
			duration: req.body.duration,
			class:[{
				classType: req.body.class[0].classType,
				price: req.body.class[0].price,
				noOfSeats: req.body.class[0].noOfSeats,
				baggageDetails:[{
						adult: req.body.class[0].baggageDetails[0].adult,
						child: req.body.class[0].baggageDetails[0].child,
						infant: req.body.class[0].baggageDetails[0].infant
				}]
			},
			{
				classType: req.body.class[1].classType,
				price: req.body.class[1].price,
				noOfSeats: req.body.class[1].noOfSeats,
				baggageDetails:[{
						adult: req.body.class[1].baggageDetails[0].adult,
						child: req.body.class[1].baggageDetails[0].child,
						infant: req.body.class[1].baggageDetails[0].infant
				}]
			},
			{
				classType: req.body.class[2].classType,
				price: req.body.class[2].price,
				noOfSeats: req.body.class[2].noOfSeats,
				baggageDetails:[{
						adult: req.body.class[2].baggageDetails[0].adult,
						child: req.body.class[2].baggageDetails[0].child,
						infant: req.body.class[2].baggageDetails[0].infant
				}]
			}]
			}).then(function(add){
				if(!add){
					res.status(400).send();
				}
				else{
					res.status(200).send(add);
				}
			});

	};
	exports.update=(req,res)=>{
		Flight.findOneAndUpdate({_id: req.params.id},{$set:{
		carrierName : req.body.carrierName,
			flightName : req.body.flightId,
			from : req.body.from,
			to : req.body.to,
			operatingDays: req.body.operatingDays,
			departureTime: req.body.departureTime,
			arrivalTime: req.body.arrivalTime,
			duration: req.body.duration,
			class:[{
				classType: req.body.class[0].classType,
				price: req.body.class[0].price,
				noOfSeats: req.body.class[0].noOfSeats,
				baggageDetails:[{
						adult: req.body.class[0].baggageDetails[0].adult,
						child: req.body.class[0].baggageDetails[0].child,
						infant: req.body.class[0].baggageDetails[0].infant
				}]
			},
			{
				classType: req.body.class[1].classType,
				price: req.body.class[1].price,
				noOfSeats: req.body.class[1].noOfSeats,
				baggageDetails:[{
						adult: req.body.class[1].baggageDetails[0].adult,
						child: req.body.class[1].baggageDetails[0].child,
						infant: req.body.class[1].baggageDetails[0].infant
				}]
			},
			{
				classType: req.body.class[2].classType,
				price: req.body.class[2].price,
				noOfSeats: req.body.class[2].noOfSeats,
				baggageDetails:[{
						adult: req.body.class[2].baggageDetails[0].adult,
						child: req.body.class[2].baggageDetails[0].child,
						infant: req.body.class[2].baggageDetails[0].infant
				}]
			}]
			}},{new: true}
			
			).then(function(add){
				if(!add){
					res.status(400).send();
				}
				else{
					res.status(200).send(add);
				}
			})
	};
	exports.show=(req,res)=>{
		Flight.find({})
			.then(function(show){
				if(show=="" && !show){
					res.status(400).send();
				}
				else{
					res.status(200).send(show);
				}
			})
	},
	exports.delete=(req,res)=>{
		Flight.findOneAndRemove({
			_id : req.params.id
		}).then(function(show){
			if(!show){
				res.status(400).send("cant delete");
			}else{
				res.status(200).send(show);
			}
		})
	},
	exports.updateOne=(req,res)=>{
		Flight.findOneAndUpdate({
				_id: req.params.id
			},{$set:{from:req.params.from}},{new: true})
			.then(function(show){
				if(!show){
					res.status(400).send("cant update");
							}
			else{
				res.status(200).send(show);
			}
		})
	};
	exports.showOne=(req,res)=>{
		Flight.find({$and:[{from:req.params.from},{to:req.params.to}]})
			// Booking.find({ "travelDate" : req.params.date}).countDocuments()

			.then(function(show){
				if(!show){
					res.status(400).send();
				}
				else{
					// Booking.find({ "travelDate" : req.params.date}).then((result)=>{console.log(result)});
					var dateCheck=req.params.date;

					console.log(dateCheck);
					var d = new Date(dateCheck);
				    var n = d.getDay();
				    console.log(n);
				    var availableArr=[];
				    console.log("show length"+show.length);
				    var strlen=show.length;
				    for(var j=0;j<show.length;j++){
				    	var str = show[j].operatingDays;
				    	var chacking=1;
					    for(var i=0;i<str.length;i++)
					    {
					    	// console.log(str.charAt(i));
						    if(/^[a-zA-Z0-9]*$/.test(str.charAt(i)) == true){
								    console.log(str.charAt(i));
								    console.log(i);
								    if(n==i){
								    	chacking=0;							
								    }
							}
					    }
					    if(chacking==0)	{	
					    	
					    		// availableArr.push(show[j]);

							
							console.log(show[j]._id);

							// const idFlight=show[j]._id;


							const economySeats=Flight.find({_id:show[j]._id})
								.then((result)=>{									
									// console.log(result[0].class[0].noOfSeats);
									return result[0].class[0].noOfSeats;
								});
								
								
							const premiumeconomySeats=Flight.find({_id:show[j]._id})
								.then((result)=>{									
									// console.log(result[0].class[0].noOfSeats);
									return result[0].class[1].noOfSeats;
								});
							const businessSeats=Flight.find({_id:show[j]._id})
								.then((result)=>{									
									// console.log(result[0].class[0].noOfSeats);
									return result[0].class[2].noOfSeats;
								});
								// Booking.find({$and:[{"travelDate" : req.params.date},{"flightId":show[j]._id},{"class":"business"}]})
								// 		.then((result)=>{
								// 			console.log("traveldate count"+result.passenger[0]);
								// 			// return result;
								// 		});	
							const businessCount=Booking.find({$and:[{"travelDate" : req.params.date},{"flightId":show[j]._id},{"class":"business"}]})
							// .countDocuments()
										.then((result)=>{
											var bcount=0;
											for(var k=0;k<result.length;k++){
												bcount=bcount+result[k].passenger.length;
											}
											//console.log("business result"+bcount);
											
											return bcount;
										});	
							// const businessSeats=Flight.distinct( "pnr" ).then(function(pnrNoArr){
							// 				return pnrNoArr[1];
							// 			});

							const premiumeconomyCount=Booking.find({$and:[{"travelDate" : req.params.date},{"flightId":show[j]._id},{"class":"premiumeconomy"}]})
							// .countDocuments()
										.then((result)=>{
											// console.log("traveldate count"+result);
											// return result;
											var pcount=0;
											for(var k=0;k<result.length;k++){
												pcount=pcount+result[k].passenger.length;
											}
											//console.log("business result"+bcount);
											
											return pcount;
										});	
							const economyCount=Booking.find({$and:[{"travelDate" : req.params.date},{"flightId":show[j]._id},{"class":"economy"}]})
							// .countDocuments()
										.then((result)=>{
											// console.log("traveldate count"+result);
											// return result;
											var ecount=0;
											for(var k=0;k<result.length;k++){
												ecount=ecount+result[k].passenger.length;
											}
											//console.log("business result"+bcount);
											
											return ecount;
										});	
								 
							// const showArr=return show[j];
							const showArr=Flight.find({_id:show[j]._id})
								.then((result)=>{
									return result;
								});



									var availableArr1=[];
									var cnt=0;	
							async function countTicket(){
								console.log("url: "+req.params.class);
								 
								let businessseats=await businessSeats;
								let premiumeconomyseats=await premiumeconomySeats;
								let economyseats=await economySeats;
								let businesscount=await businessCount;
								let premiumeconomycount=await premiumeconomyCount;
								let economycount=await economyCount;
								// let idflight=await idFlight;
								let showarr=await showArr;
								console.log(economyseats);
								console.log(premiumeconomyseats);
								console.log(businessseats);
								// console.log(idflight);
								console.log("economycount: "+economycount);
								console.log("premiumeconomycount: "+premiumeconomycount);
								console.log("businesscount: "+businesscount);
								// console.log(showarr);
								
								// console.log(availableArr1);
								// Flight.find({_id:show[j]._id}).then((result)=>{
								// 	console.log(result);
								// });
								// console.log(show[j]);
								cnt=cnt+1;
								if(req.params.class==="economy"){
									if(economycount<economyseats){
										console.log("length:"+strlen);
										if(cnt===strlen){
											console.log("cnt"+cnt);
											availableArr1.push(showarr[0]);
											return availableArr1;
										}
										else{
											availableArr1.push(showarr[0]);
										}
									}else{
										if(cnt===strlen){
											return availableArr1;
										}
										// console.log("if cancelled");
									}
								}
								
								if(req.params.class==="premiumeconomy"){
									if(premiumeconomycount<premiumeconomyseats ){
										console.log("length:"+strlen);
										if(cnt===strlen){
											console.log("cnt"+cnt);
											availableArr1.push(showarr[0]);
											return availableArr1;
										}
										else{
											availableArr1.push(showarr[0]);
										}
									}else{
										if(cnt===strlen){
											return availableArr1;
										}
										// console.log("if cancelled");
									}
								}
								
								if(req.params.class==="business")
								{
									// console.log(showarr[0]);
									if(businesscount<businessseats ){
										console.log("length:"+strlen);
										console.log("cnt"+cnt);
										if(cnt===strlen){
											
											availableArr1.push(showarr[0]);
											return availableArr1;
										}
										else{
											availableArr1.push(showarr[0]);
										}
									}else{
										if(cnt===strlen){
											return availableArr1;
										}
										// console.log("if cancelled");
									}
								}
								
								
								

							}
							var cnt1=0;	
							countTicket()
								.then((result)=>{
									// availableArr1.push(result);
									cnt1=cnt1+1;
									console.log(cnt1);
									console.log(result);
									if(cnt1===strlen){
										res.status(200).send(result);
									}
									// if(result==="undefined"){
									// 	console.log(result);
									// }else{
									// 	res.status(200).send(result);
									// }
									
								});
							// res.status(200).send(availableArr1);
					    }else{
					    	strlen=strlen-1;
					    }
					    
				    }
				    
    				
					// res.status(200).send(availableArr);
				}
			});
	};
	exports.test=(req,res)=>{
		Flight.find({ "flightName" : req.params.date})
		.then((result)=>{console.log(result)});						 
	};

