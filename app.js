// Dependencies
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise=global.Promise;

//Routes
var routes = require('./routes/routes');

//Database config
var db = require('./config/db');

//Express
var app = express();

//MongoDB
mongoose.connect(db.url);

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
routes(app);

//Return app
module.exports=app;